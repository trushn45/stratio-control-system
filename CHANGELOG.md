# Changelog

All notable changes to this project will be documented in this file.

## [1.0.0] - 2018-04-27
### Added
- New batch to process the Control System Register from Tuesday to Saturday, and send notifications to kafka queue
- Get a Timesheet list in a dates range
- Get a Control User time