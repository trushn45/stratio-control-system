package com.stratio.controlsystem.repository;

import com.stratio.controlsystem.ControlSystemApplication;
import com.stratio.controlsystem.entity.ControlSystemEntity;
import com.stratio.controlsystem.entity.UserEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

@SpringBootTest(classes = ControlSystemApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class UserRepositoryTest {


	@Autowired
	private UserRepository userRepository;

	@Test
	public void givenUser_whenSaveUser_thenReturnUserSaved() {
		UserEntity user = userRepository.save(createUser());
		assertNotNull(user);
		assertTrue("John".equalsIgnoreCase(user.getName()));
		assertTrue(1 == user.getTimesUsedFingerprint().size());
	}

	@Test
	public void givenUserAndDatesRange_whenFindUser_thenReturnUserWithInterval() {
		UserEntity user = userRepository.save(createUser());
		UserEntity userFind = userRepository.findByIdAndTimesUsedFingerprint(user.getId(), LocalDateTime.now().minusDays(1L), LocalDateTime.now().plusDays(1L));
		assertNotNull(user);
		assertTrue("John".equalsIgnoreCase(user.getName()));
		assertTrue(1 == user.getTimesUsedFingerprint().size());
	}

	private UserEntity createUser() {
		UserEntity user = new UserEntity();
		user.setId(2L);
		user.setId(1L);
		user.setEmail("a@a.es");
		user.setName("John");
		user.setRole("user");
		user.setSurname("Smith");

		ControlSystemEntity controlSystemEntity = new ControlSystemEntity();
		controlSystemEntity.setStartTime(LocalDateTime.now());
		Set<ControlSystemEntity> controlSystemEntitySet = new HashSet<>(Arrays.asList(controlSystemEntity));
		user.setTimesUsedFingerprint(controlSystemEntitySet);
		return user;
	}
}
