package com.stratio.controlsystem.batch;

import com.stratio.controlsystem.entity.ControlSystemEntity;
import com.stratio.controlsystem.entity.UserEntity;
import com.stratio.controlsystem.service.NotificationService;
import com.stratio.controlsystem.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BatchTest {

	@Mock
	private UserService userService;

	@Mock
	private NotificationService notificationService;

	@InjectMocks
	private NotificationTaskCreator notificationTaskCreator;

	@Test
	public void givenAControlSystemList_whenProcessControlSystemList_thenSendNotifications() {
		UserEntity userEntity = createUserTimeSheet();
		List<UserEntity> users = Arrays.asList(userEntity);
		LocalDateTime day = LocalDate.now().atStartOfDay().minusDays(1L);
		LocalDateTime endDay = LocalDate.now().atTime(LocalTime.MAX).minusDays(1L);

		when(userService.findAllByTimesUsedFingerprintRangeDates(day, endDay)).thenReturn(users);
		notificationTaskCreator.create();
		verify(notificationService, atLeast(1)).sendMessage(any());
	}


	private UserEntity createUser() {
		UserEntity user = new UserEntity();
		user.setId(1L);
		user.setEmail("a@a.es");
		user.setName("John");
		user.setRole("user");
		user.setSurname("Smith");
		return user;
	}

	private UserEntity createUserTimeSheet() {
		UserEntity user = createUser();

		ControlSystemEntity controlSystemEntity = new ControlSystemEntity();
		controlSystemEntity.setStartTime(LocalDateTime.now().minusDays(1L));
		controlSystemEntity.setCloseTime(LocalDateTime.now().minusDays(1L).plusMinutes(60L));
		Set<ControlSystemEntity> controlSystemEntitySet = new HashSet<>(Arrays.asList(controlSystemEntity));
		user.setTimesUsedFingerprint(controlSystemEntitySet);
		return user;
	}
}
