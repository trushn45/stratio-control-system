package com.stratio.controlsystem.controller;

import com.stratio.controlsystem.ControlSystemApplication;
import com.stratio.controlsystem.entity.ControlSystemEntity;
import com.stratio.controlsystem.entity.UserEntity;
import com.stratio.controlsystem.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = { ControlSystemApplication.class })
@ActiveProfiles("test")
public class ControlSystemControllerIT {

	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	private UserService userService;

	private MockMvc mockMvc;

	@Before
	public void setUpMockMvc() {
		UserEntity user = userService.save(createUser());
		mockMvc = webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void givenUserIdAndDateRange_whenGetTimeControl_thenReturnOk() throws Exception {

		mockMvc.perform(get("/control-system/time-control/1")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.param("fromDay", "2018-04-04")
				.param("toDay", "2018-05-28"))
				.andExpect(status().isOk());

	}

	@Test
	public void givenUserIdAndDateRange_whenGetTimeControl_thenReturnException() throws Exception {

		mockMvc.perform(get("/control-system/time-control/1")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.param("fromDay", "2011-04-04")
				.param("toDay", "2011-05-28"))
				.andExpect(status().is4xxClientError());
	}

	@Test
	public void givenUserIdAndDateRange_whenGetTimeSheet_thenReturnTimeSheetList() throws Exception {

		LocalDate date = LocalDate.now().minusDays(2l);
		String starDate = date.format(formatter);
		date = LocalDate.now().plusDays(1l);
		String endDate = date.format(formatter);

		mockMvc.perform(get("/control-system/time-sheet/1")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.param("fromDay", starDate)
				.param("toDay", endDate))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(4)))
				.andExpect(jsonPath("$[1].totalHourDay", is(2)));
	}

	@Test
	public void givenUserIdAndDateRange_whenGetTimeSheetDoesNotExist_thenReturnNotFound() throws Exception {

		LocalDate date = LocalDate.now().minusDays(2l);
		String starDate = date.format(formatter);
		date = LocalDate.now().plusDays(1l);
		String endDate = date.format(formatter);

		mockMvc.perform(get("/control-system/time-sheet/1")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.param("fromDay", "2011-04-04")
				.param("toDay", "2011-04-04"))
				.andExpect(status().is4xxClientError());
	}

	private UserEntity createUser() {
		UserEntity user = new UserEntity();
		user.setId(1L);
		user.setEmail("a@a.es");
		user.setName("John");
		user.setRole("user");
		user.setSurname("Smith");

		ControlSystemEntity controlSystemEntity = new ControlSystemEntity();
		controlSystemEntity.setStartTime(LocalDateTime.now().minusDays(1l));
		controlSystemEntity.setCloseTime(LocalDateTime.now().minusDays(1l).plusHours(2));
		Set<ControlSystemEntity> controlSystemEntitySet = new HashSet<>(Arrays.asList(controlSystemEntity));
		user.setTimesUsedFingerprint(controlSystemEntitySet);
		return user;
	}
}
