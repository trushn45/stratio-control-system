package com.stratio.controlsystem.service;

import com.stratio.controlsystem.entity.ControlSystemEntity;
import com.stratio.controlsystem.entity.UserEntity;
import com.stratio.controlsystem.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

	@InjectMocks
	private UserService userService;

	@Mock
	private UserRepository userRepository;

	@Test
	public void givenUser_whenSaveUser_thenReturnUserSaved() {
		UserEntity createUser = createUser();
		when(userRepository.save(createUser)).thenReturn(createUser);
		UserEntity user = userService.save(createUser);
		assertNotNull(user);
		assertTrue("John".equalsIgnoreCase(user.getName()));
		assertTrue(1 == user.getTimesUsedFingerprint().size());
	}

	@Test
	public void givenUserAndDatesRange_whenFindUser_thenReturnUserWithInterval() {
		UserEntity createUser = createUser();
		when(userRepository.save(createUser)).thenReturn(createUser);
		UserEntity user = userService.save(createUser);
		when(userRepository.findByIdAndTimesUsedFingerprint(user.getId(), LocalDateTime.now().minusDays(1L),
				LocalDateTime.now().plusDays(1L))).thenReturn(user);
		UserEntity userFind = userRepository
				.findByIdAndTimesUsedFingerprint(user.getId(), LocalDateTime.now().minusDays(1L),
						LocalDateTime.now().plusDays(1L));
		assertNotNull(user);
		assertTrue("John".equalsIgnoreCase(user.getName()));
		assertTrue(1 == user.getTimesUsedFingerprint().size());
	}

	private UserEntity createUser() {
		UserEntity user = new UserEntity();
		user.setId(1L);
		user.setEmail("a@a.es");
		user.setName("John");
		user.setRole("user");
		user.setSurname("Smith");

		ControlSystemEntity controlSystemEntity = new ControlSystemEntity();
		controlSystemEntity.setStartTime(LocalDateTime.now());
		Set<ControlSystemEntity> controlSystemEntitySet = new HashSet<>(Arrays.asList(controlSystemEntity));
		user.setTimesUsedFingerprint(controlSystemEntitySet);
		return user;
	}
}


