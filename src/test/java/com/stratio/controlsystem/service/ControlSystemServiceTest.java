package com.stratio.controlsystem.service;

import com.stratio.controlsystem.dto.User;
import com.stratio.controlsystem.dto.mapper.UserMapper;
import com.stratio.controlsystem.entity.ControlSystemEntity;
import com.stratio.controlsystem.dto.TimeSheet;
import com.stratio.controlsystem.entity.UserEntity;
import com.stratio.controlsystem.exception.UserNotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static junit.framework.TestCase.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ControlSystemServiceTest {

	@Mock
	private UserService userService;

	@InjectMocks
	private ControlSystemService controlSystemService;

	private UserMapper userMapper;


	@Before
	public void setUp() {
		this.userMapper = new UserMapper();
		controlSystemService = new ControlSystemService(userService, userMapper);
	}

	@Test(expected = UserNotFoundException.class)
	public void givenWrongDatesAndUserId_whenIWantTimeControl_thenReturnUserUserAbsent() {
		when(userService.findByUserIdAndDateRange(1L,
				LocalDate.now().minusDays(1L).atStartOfDay(),
				LocalDate.now().plusDays(1l).atTime(LocalTime.MAX))).thenReturn(createUserAbsent());

		User user = controlSystemService
				.timeControl(1l,LocalDate.now().minusDays(1l), LocalDate.now().plusDays(1l));
		assertNotNull(user);
		assertFalse(user.getIsPresent());
		assertTrue(1 == user.getTimesUsedFingerprint().size());
	}

	@Test
	public void givenStartDateEndDateAndUserId_whenIWantTimeControl_thenReturnUserNotFound() {
		LocalDateTime startDay = LocalDate.now().minusDays(4L).atStartOfDay();
		LocalDateTime endDay = LocalDate.now().atTime(LocalTime.MAX);
		UserEntity user = createUserTimeSheet();

		when(userService.findByUserIdAndDateRange(1L,
				startDay, endDay)).thenReturn(user);
		User userControl = controlSystemService
				.timeControl(1l,startDay.toLocalDate(), endDay.toLocalDate());;
		assertNotNull(userControl);
		assertFalse(userControl.getIsPresent());
		assertTrue(1 == userControl.getTimesUsedFingerprint().size());
		assertTrue("John".equalsIgnoreCase(userControl.getName()));
	}

	@Test
	public void givenStartDateEndDateAndUserId_whenIWantUserTimeSheet_thenReturnCorrectUserTimeSheet() {
		LocalDateTime startDay = LocalDate.now().minusDays(4L).atStartOfDay();
		LocalDateTime endDay = LocalDate.now().atTime(LocalTime.MAX);
		UserEntity user = createUserTimeSheet();

		when(userService.findByUserIdAndDateRange(1L,
				startDay, endDay)).thenReturn(user);

		List<TimeSheet> timeSheets = controlSystemService
				.timeSheetOfWorkedHours(1L,startDay.toLocalDate(), endDay.toLocalDate());
		assertNotNull(timeSheets);
		assertTrue(5 == timeSheets.size());
		assertTrue(1 == timeSheets.get(2).getTotalHourDay());

	}

	@Test(expected = UserNotFoundException.class)
	public void givenUserIdWithWrongRanges_whenIWantUserTimeSheet_thenReturnUserNotFoundException() {
		LocalDateTime startDay = LocalDate.now().minusDays(100L).atStartOfDay();
		LocalDateTime endDay = LocalDate.now().minusDays(90L).atTime(LocalTime.MAX);

		when(userService.findByUserIdAndDateRange(1L,
				startDay, endDay)).thenReturn(null);

		List<TimeSheet> timeSheets = controlSystemService
				.timeSheetOfWorkedHours(1L,startDay.toLocalDate(), endDay.toLocalDate());
	}

	private UserEntity createUser() {
		UserEntity user = new UserEntity();
		user.setId(1L);
		user.setEmail("a@a.es");
		user.setName("John");
		user.setRole("user");
		user.setSurname("Smith");
		return user;
	}

	private UserEntity createUserAbsent() {
		UserEntity user = createUser();
		ControlSystemEntity controlSystemEntity = new ControlSystemEntity();
		controlSystemEntity.setId(9l);
		controlSystemEntity.setStartTime(LocalDateTime.now());
		controlSystemEntity.setCloseTime(LocalDateTime.now().plusMinutes(10l));
		Set<ControlSystemEntity> controlSystemEntitySet = new HashSet<>(Arrays.asList(controlSystemEntity));
		user.setTimesUsedFingerprint(controlSystemEntitySet);
		return user;
	}

	private UserEntity createUserTimeSheet() {
		UserEntity user = createUser();

		ControlSystemEntity controlSystemEntity = new ControlSystemEntity();
		controlSystemEntity.setStartTime(LocalDateTime.now().minusDays(2L));
		controlSystemEntity.setCloseTime(LocalDateTime.now().minusDays(2L).plusMinutes(60L));
		Set<ControlSystemEntity> controlSystemEntitySet = new HashSet<>(Arrays.asList(controlSystemEntity));
		user.setTimesUsedFingerprint(controlSystemEntitySet);
		return user;
	}
}
