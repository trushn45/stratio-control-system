package com.stratio.controlsystem.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * User information
 */
@Setter
@Getter
public class User {

	private String name;

	private String surname;

	private String role;

	private String email;

	private Boolean isPresent;

	private List<ControlSystem> timesUsedFingerprint;
}
