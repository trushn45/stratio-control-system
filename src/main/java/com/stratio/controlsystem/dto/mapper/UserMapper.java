package com.stratio.controlsystem.dto.mapper;

import com.stratio.controlsystem.dto.User;
import com.stratio.controlsystem.entity.UserEntity;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.stereotype.Component;

/**
 * Orikas's mapper convert
 */
@Component
public class UserMapper  extends ConfigurableMapper {

	final static MapperFactory MAPPER_FACTORY = new DefaultMapperFactory.Builder().build();

	private MapperFacade mapperFacade;

	@Override
	public void configure(MapperFactory factory) {

		factory.classMap(UserEntity.class, User.class)
				.byDefault()
				.register();
	}


}
