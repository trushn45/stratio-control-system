package com.stratio.controlsystem.dto;

import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

/**
 * This class has the information to show in the API
 */
@Getter
@Builder
public class ControlSystem {

	private Long id;

	private LocalDateTime startTime;

	private LocalDateTime closeTime;

}
