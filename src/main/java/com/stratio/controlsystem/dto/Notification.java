package com.stratio.controlsystem.dto;

import lombok.Builder;
import lombok.Getter;

/**
 * Notification to send
 */
@Builder
@Getter
public class Notification {

	private User user;

	private String message;

}
