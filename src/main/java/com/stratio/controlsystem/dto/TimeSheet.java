package com.stratio.controlsystem.dto;

import lombok.Builder;
import lombok.Getter;

import java.time.LocalDate;

/**
 * Timesheet information
 */
@Builder
@Getter
public class TimeSheet {

	private User user;

	private LocalDate day;

	private int totalHourDay;
}
