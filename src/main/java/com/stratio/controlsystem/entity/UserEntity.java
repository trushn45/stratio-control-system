package com.stratio.controlsystem.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * User entity
 */
@Getter
@Setter
@Entity
public class UserEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	private String fingerprint;

	private String name;

	private String surname;

	private String role;

	private String email;

	@ManyToMany(cascade = CascadeType.ALL , fetch = FetchType.EAGER)
	@JoinTable(
			name = "User_ControlSystem",
			joinColumns = { @JoinColumn(name = "user_id") },
			inverseJoinColumns = { @JoinColumn(name = "controlsystem_id") }
	)
	Set<ControlSystemEntity> timesUsedFingerprint = new LinkedHashSet<>();

}
