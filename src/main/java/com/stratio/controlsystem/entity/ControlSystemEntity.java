package com.stratio.controlsystem.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * Control system entity
 */
@Getter
@Setter
@Entity
public class ControlSystemEntity {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long id;

	@Column
	private LocalDateTime startTime;

	@Column
	private LocalDateTime closeTime;

	@ManyToMany(mappedBy = "timesUsedFingerprint")
	private Set<UserEntity> usersControlSystem = new HashSet<>();
}
