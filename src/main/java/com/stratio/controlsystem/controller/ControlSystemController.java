package com.stratio.controlsystem.controller;

import com.stratio.controlsystem.dto.User;
import com.stratio.controlsystem.dto.TimeSheet;
import com.stratio.controlsystem.service.ControlSystemService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping(value = "/control-system")
public class ControlSystemController {

	private final ControlSystemService controlSystemService;

	public ControlSystemController(ControlSystemService controlSystemService) {
		this.controlSystemService = controlSystemService;
	}

	@GetMapping(value = "time-control/{userId}")
	public ResponseEntity<User> getTimeControlByUserRangeDates(@PathVariable Long userId,
			@RequestParam(name = "fromDay") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDay,
			@RequestParam(name = "toDay") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate toDay) {
		User user = controlSystemService.timeControl(userId, fromDay, toDay);
		return ResponseEntity.ok(user);
	}

	@GetMapping(value = "time-sheet/{userId}")
	public ResponseEntity<List<TimeSheet>> getTimeSheetByUserRangeDates(@PathVariable Long userId,
			@RequestParam(name = "fromDay") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDay,
			@RequestParam(name = "toDay") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate toDay) {
		List<TimeSheet> timeSheets = controlSystemService.timeSheetOfWorkedHours(userId, fromDay, toDay);
		return ResponseEntity.ok(timeSheets);
	}


}
