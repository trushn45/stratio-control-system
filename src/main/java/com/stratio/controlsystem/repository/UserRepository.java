package com.stratio.controlsystem.repository;

import com.stratio.controlsystem.entity.UserEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Long>{

	public UserEntity findByFingerprint(String fingerprint);

	@Query("SELECT u FROM UserEntity u join fetch u.timesUsedFingerprint c WHERE u.id = :id and c.startTime >:startTime and c.closeTime <:closeTime")
	public UserEntity findByIdAndTimesUsedFingerprint(@Param("id")Long userId,  @Param("startTime") LocalDateTime starTime, @Param("closeTime") LocalDateTime closeTime);

	@Query("SELECT u FROM UserEntity u join fetch u.timesUsedFingerprint c WHERE c.startTime >:startTime and c.closeTime <:closeTime")
	public List<UserEntity> findAllByTimesUsedFingerprintRangeDates( @Param("startTime") LocalDateTime starTime, @Param("closeTime") LocalDateTime closeTime);
}
