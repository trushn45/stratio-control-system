package com.stratio.controlsystem.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class UserNotFoundException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = -6384770197056630255L;

	/**
	 * Constructs a <code>UserNotFoundException</code> with the specified message.
	 *
	 * @param msg the detail message.
	 */
	public UserNotFoundException(String msg) {
		super(msg);
	}

	/**
	 * Constructs a {@code UserNotFoundException} with the specified message and root
	 * cause.
	 *
	 * @param msg the detail message.
	 * @param t   root cause
	 */
	public UserNotFoundException(String msg, Throwable t) {
		super(msg, t);
	}
}