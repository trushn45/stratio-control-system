package com.stratio.controlsystem.batch;

import com.stratio.controlsystem.dto.Notification;
import com.stratio.controlsystem.entity.ControlSystemEntity;
import com.stratio.controlsystem.entity.UserEntity;
import com.stratio.controlsystem.service.NotificationService;
import com.stratio.controlsystem.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Set;

/**
 * This class is in charge on throw a procces from Tuesday to Saturday at 11 30, this proccess depends on:
 * <ul>
 *     <li>The user has got in before bussines hour</li>
 *     <li>The user has got out after bussines hour</li>
 *     <li>The user has worked less than 8 hours</li>
 * </ul>
 */
@Component
@Slf4j
public class NotificationTaskCreator {

	private final UserService userService;

	private final NotificationService notificationService;

	public NotificationTaskCreator(UserService userService, NotificationService notificationService) {
		this.userService = userService;
		this.notificationService = notificationService;
	}

	/**
	 * This is a daily proccess wich it is throwed every day from Tuesday to saturday at 11:30, and analyze the day before
	 */
	@Scheduled(cron = "0 30 11 ? * TUE-SAT")
	public void create() {
		final LocalDateTime start = LocalDateTime.now();
		LocalDateTime day = LocalDate.now().atStartOfDay().minusDays(1l);
		LocalDateTime endDay = LocalDate.now().atTime(LocalTime.MAX).minusDays(1l);

		List<UserEntity> userList = userService.findAllByTimesUsedFingerprintRangeDates(day, endDay);
		userList.forEach(user -> {
			proccesControlSystem(user.getId(), user.getTimesUsedFingerprint());

		});
		log.info("Event has been throwed at {}", start);
	}

	/**
	 * This method proccess every control system registered into database, and sent a notification, this depends on:
	 * <ul>
	 *     <li>The user has got in before bussines hour</li>
	 *     <li>The user has got out after bussines hour</li>
	 *     <li>The user has worked less than 8 hours</li>
	 * </ul>
	 * @param userId denotes the user by his long id
	 * @param controlSystem a list with the register of the day before
	 */
	private void proccesControlSystem(Long userId, Set<ControlSystemEntity> controlSystem) {

		controlSystem.forEach(cs -> {
			Long totalHours = null;
			Integer index = null;
			if (totalHours == null) {
				totalHours = cs.getStartTime().until(cs.getCloseTime(), ChronoUnit.HOURS);
				index = 1;
			}
			if ((cs.getStartTime().isBefore(LocalDateTime.now().toLocalDate().atTime(8, 0)))
					|| (cs.getStartTime().isAfter(LocalDateTime.now().toLocalDate().atTime(10, 0)))) {
				String message = "User " + userId + "has arrived to the office before or after the bussines hour";
				notificationService.sendMessage(createNotification(message));
				log.info("The user {}  has arrived to the office before or after the business hour", userId);
			} else if (cs.getCloseTime()
					.isAfter(LocalDateTime.now().toLocalDate().atTime(18, 0))) {
				String message = "User " + userId + "has left the office after the bussines hour";
				notificationService.sendMessage(createNotification(message));
				log.info("The user {}  has left the office after the business hour", userId);
			}
			 totalHours = cs.getStartTime().until(cs.getCloseTime(), ChronoUnit.HOURS) + totalHours;

			if (index == controlSystem.size()) {
				if (totalHours > 8 || totalHours < 8) {
					String message = "User " + userId + " in the day " + cs.getStartTime().toLocalDate()
							+ "has worked more or less than 8 hours";
					notificationService.sendMessage(createNotification(message));

				}
			}
			index++;
		});

	}

	private Notification createNotification(String message) {
		return Notification.builder()
				.message(message).build();
	}
}
