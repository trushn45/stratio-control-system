package com.stratio.controlsystem.service;

import com.stratio.controlsystem.dto.User;
import com.stratio.controlsystem.dto.mapper.UserMapper;
import com.stratio.controlsystem.entity.ControlSystemEntity;
import com.stratio.controlsystem.dto.TimeSheet;
import com.stratio.controlsystem.entity.UserEntity;
import com.stratio.controlsystem.exception.UserNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Stream;

import static java.lang.Math.toIntExact;

/**
 * This class is in charge of generate the information about the worked hours and the presence of the user
 */
@Service
@Slf4j
public class ControlSystemService {

	private final UserService userService;

	private final UserMapper userMapper;

	public ControlSystemService(UserService userService, UserMapper userMapper) {
		this.userService = userService;
		this.userMapper = userMapper;
	}

	/**
	 *
	 * @param userId userId denotes the user by his long id
	 * @param startDate startDate first day of timesheet list
	 * @param endDate endDate last day of timesheet list
	 * @return the user information
	 */
	public User timeControl(Long userId, LocalDate startDate, LocalDate endDate) {
		UserEntity user = userService
				.findByUserIdAndDateRange(userId,
						startDate.atStartOfDay(), endDate.atTime(LocalTime.MAX));

		if (user == null) {
			log.info("Control system not found for user{}, in date from {} to{}", userId, startDate, endDate);
			throw new UserNotFoundException("Time Control with theses dates has not been found");
		}
		Optional<ControlSystemEntity> controlSystem = user.getTimesUsedFingerprint().stream()
				.filter(Objects::nonNull)
				.max(Comparator.comparing(ControlSystemEntity::getStartTime))
				.filter(cs -> cs.getStartTime() != null && cs.getCloseTime() == null);

		User userDto = userMapper.map(user, User.class);
		if (controlSystem.isPresent()) {
			userDto.setIsPresent(Boolean.TRUE);
		} else {
			userDto.setIsPresent(Boolean.FALSE);
		}
		return userDto;
	}

	/**
	 * Return a timesheet list with the number of worked hours every day
	 * @param userId denotes the user by his long id
	 * @param startDate first day of timesheet list
	 * @param endDate last day of timesheet list
	 * @return Selected list of timesheet
	 * @throws UserNotFoundException when <code>userId></code> or <code>startDate</code> and <code>endDate</code> are not known
	 */
	public List<TimeSheet> timeSheetOfWorkedHours(Long userId, LocalDate startDate, LocalDate endDate) {
		UserEntity user = userService
				.findByUserIdAndDateRange(userId, startDate.atStartOfDay(), endDate.atTime(LocalTime.MAX));

		if (user == null) {
			log.info("Don't existe information for the user with startDate{} and endDate{} for user{}", userId, startDate, endDate);
			throw new UserNotFoundException("Timesheet with this range has not been found");
		}
		List<TimeSheet> timeSheetList = new ArrayList<>();

		Stream.iterate(startDate, date -> date.plusDays(1))
				.limit(ChronoUnit.DAYS.between(startDate, endDate) + 1).forEach(day -> {

			Integer totalHour = user.getTimesUsedFingerprint().stream()
					.filter(cs -> day.isEqual(cs.getStartTime().toLocalDate()))
					.map(cs -> toIntExact(Duration.between(cs.getStartTime(), cs.getCloseTime()).toHours()))
					.reduce(0, (s1, s2) -> s1 + s2, (p, q) -> p + q);
			TimeSheet timeSheet = createTimeSheet(day, totalHour, userMapper.map(user, User.class));
			timeSheetList.add(timeSheet);
		});

		return timeSheetList;
	}

	private TimeSheet createTimeSheet(LocalDate day, Integer totalHours, User user) {
		return TimeSheet.builder()
				.user(user)
				.day(day)
				.totalHourDay(totalHours)
				.build();
	}
}
