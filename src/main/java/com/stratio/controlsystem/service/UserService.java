package com.stratio.controlsystem.service;

import com.stratio.controlsystem.entity.UserEntity;
import com.stratio.controlsystem.repository.UserRepository;
import org.assertj.core.util.Lists;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * User service information
 */
@Service
public class UserService {

	private final UserRepository userRepository;

	public UserService (UserRepository userRepository){
		this.userRepository = userRepository;
	}

	public UserEntity save(UserEntity user) {
		return userRepository.save(user);
	}

	public UserEntity findByFingerprint(String fingerprint) {
		return userRepository.findByFingerprint(fingerprint);
	}

	public UserEntity findByUserIdAndDateRange(Long userId,
			LocalDateTime starTime, LocalDateTime endDate) {
		return userRepository.findByIdAndTimesUsedFingerprint(userId, starTime, endDate);
	}

	public List<UserEntity> findAllByTimesUsedFingerprintRangeDates(LocalDateTime starTime, LocalDateTime endDate) {
		return Lists.newArrayList(userRepository.findAllByTimesUsedFingerprintRangeDates(starTime, endDate));
	}

}
