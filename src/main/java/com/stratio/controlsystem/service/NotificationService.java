package com.stratio.controlsystem.service;

import com.stratio.controlsystem.dto.Notification;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

/**
 * Send messages to a queue kafka
 */
@Service
@Slf4j
public class NotificationService {

	@Value(value = "${kafka.topic}")
	private String topic;

	@Autowired
	private KafkaTemplate<String, Notification> kafkaTemplate;

	public void sendMessage(Notification notification) {
		kafkaTemplate.send(topic, notification);
	}
}
