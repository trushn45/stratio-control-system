# Stratio Control System

The aim of this project is provide a control system in the company.


## How does it work?
### This project has three different functionalites:
 * Get User Time Control
 * Get Timesheet between two dates
 * Create a batch process to analyze every register from Monday to Satuarday at 11 30
    
## How does it run?
   
If you want to run it:


using java:
```
$ java -jar target/stratio-contrl-system-1.0.0.jar
```
from maven:
```
$ mvn spring-boot:run
```
   